#!/bin/bash

export DOCKER_CLI_EXPERIMENTAL=enabled

docker manifest create $QUAY_REPO/test-gitlab:multi-arch-gitlab-latest \
$QUAY_REPO/test-gitlab:multi-arch-gitlab-x86_64 $QUAY_REPO/test-gitlab:multi-arch-gitlab-ppc64le

docker manifest inspect $QUAY_REPO/test-gitlab:multi-arch-gitlab-latest

docker login quay.io -u "$ROBOT_USER" -p $ROBOT_TOKEN

docker manifest push $QUAY_REPO/test-gitlab:multi-arch-gitlab-latest
